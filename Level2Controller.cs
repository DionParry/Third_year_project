﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level2Controller : MonoBehaviour {

	//public GameObject BlockAProgressWall;

	[Header("Moving Pad 1 settings")]
	public GameObject movingPad;
	public Vector3 pos1 = new Vector3(); //z: 51.5 x: 3, -3
	public Vector3 pos2 = new Vector3();

	[Header("Moving Pad 2 settings")]
	public GameObject movingPad2;
	public Vector3 pos3 = new Vector3(); //z 54.5
	public Vector3 pos4 = new Vector3();
	public float moveSpeed;

	public GameObject BlockAProgressWall;
	public GameObject BlockAText;
	public GameObject BlockAExitText;
	public GameObject BlockBProgressWall;
	public GameObject BlockBText;
	public GameObject BlockCProgressWall;
	public GameObject BlockCText;
	//public GameObject BlockDProgressWall;
	//public GameObject BlockDText;
	public GameObject BlockEProgressWall;
	public GameObject BlockEText;
	public GameObject BlockEText2;
	//public GameObject BlockFText;

	// Use this for initialization
	// Turn all other text other than current room off
	void Start () {
		BlockBText.SetActive (false);
		BlockCText.SetActive (false);

		BlockEText.SetActive (false);
		BlockEText2.SetActive (false);
		//BlockFText.SetActive (false);
	}
	
	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update () {
		MoveObjectPosition (movingPad, pos1, pos2, moveSpeed);
		MoveObjectPosition (movingPad2, pos3, pos4, moveSpeed);
	}

	/// <summary>
	/// Raises the trigger enter event.
	/// Handle level progression
	/// When object with current area tag is collected
    /// Wall is deactivated and progress can be made to next area
	/// </summary>
	/// <param name="other">Other.</param>
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag ("BlockAProgressCube")) {
			other.gameObject.SetActive (false);
			BlockProgression (BlockAProgressWall, BlockBText, BlockAText);
			BlockAExitText.SetActive (false);
		}
		if (other.gameObject.CompareTag ("BlockBProgressCube")) {
			other.gameObject.SetActive (false);
			BlockProgression (BlockBProgressWall, BlockCText, BlockBText);
		}
		if (other.gameObject.CompareTag ("BlockCProgressCube")) {
			other.gameObject.SetActive (false);
			BlockProgression (BlockCProgressWall, BlockEText, BlockCText);
		}
		if (other.gameObject.CompareTag ("BlockEProgressCube")) {
			other.gameObject.SetActive (false);
			BlockProgression (BlockEProgressWall, BlockEText, BlockEText);
			BlockEText2.SetActive (false);
		}
		/*
		if (other.gameObject.CompareTag ("BlockEProgressCube")) {
			other.gameObject.SetActive (false);
			BlockEProgression ();
		}
		if (other.gameObject.CompareTag ("Exit Plane")) {
			SceneManager.LoadScene (0);
			level1Completed = true;
		}
		*/
	}

	/// <summary>
	/// Blocks the progression.
	/// </summary>
	/// <param name="wall">Wall.</param>
	/// <param name="enableText">Enable text.</param>
	/// <param name="disableText">Disable text.</param>
	void BlockProgression(GameObject wall, GameObject enableText, GameObject disableText) {
		wall.SetActive (false);
		enableText.SetActive (true);
		disableText.SetActive(false);
	}

	/// <summary>
	/// Moves the object position.
	/// </summary>
	/// <param name="gameObject">Game object.</param>
	/// <param name="p1">P1.</param>
	/// <param name="p2">P2.</param>
	/// <param name="speed">Speed.</param>
	void MoveObjectPosition(GameObject gameObject, Vector3 p1, Vector3 p2, float speed) {
		gameObject.transform.position = Vector3.Lerp (p1, p2, Mathf.PingPong(Time.time * speed, 1.0f));
	}


}
