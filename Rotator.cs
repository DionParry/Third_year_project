﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	/// <summary>
	/// Update rotation of this instance.
	/// </summary>
	void Update () {

		transform.Rotate (new Vector3 (15, 30, 45) * Time.deltaTime);
	}
}
