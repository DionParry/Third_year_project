﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ExitLevel : MonoBehaviour
{
	
	Teleport t;
	TimeControl tc;
	public GameObject ExitPanel;
	public Text debugText;

	// Use this for initialization
	void Start ()
	{
		ExitPanel.SetActive (false);
		//call scripts
		t = GameObject.FindObjectOfType<Teleport> ();
		tc = GameObject.FindObjectOfType<TimeControl> ();
	}
	
	// Once abilities are all collected, exit panel becomes active
	void Update ()
	{
		if (t.teleportPickUp != false && tc.TimeControlPickUp != false) {
			ExitPanel.SetActive (true);
		}
	}

	//returns player to home screen
	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.CompareTag ("Exit Plane")) {
			debugText.text = ("Collision");
			SceneManager.LoadScene (0);
		}
	}
}
