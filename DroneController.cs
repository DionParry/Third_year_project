﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;
using UnityEngine.UI;

public class DroneController : MonoBehaviour {

	public float speed;
	public Camera droneCamera;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		OVRInput.Update ();
		TouchPad ();
		keyController ();
	}
		
	//Testing in unity
	void keyController() {
		if (droneCamera.depth == 1) {
			if (Input.GetKey (KeyCode.J)) {
				transform.position = transform.position + droneCamera.transform.forward * speed * Time.deltaTime;
			}
		}
	}

	//setting up controller
	void TouchPad ()
	{
		if (OVRInput.GetActiveController () == OVRInput.Controller.LTrackedRemote ||
			OVRInput.GetActiveController () == OVRInput.Controller.RTrackedRemote) {
			// yes, are they touching the touchpad?
			if (OVRInput.Get (OVRInput.Touch.PrimaryTouchpad)) {
				// yes, let's require an actual click rather than just a touch.
				if (OVRInput.Get (OVRInput.Button.PrimaryTouchpad)) {
					// button is depressed, handle the touch.
					TouchPadSetup();
				}
			}
		}
	}

	//Config for dpad
	void TouchPadSetup() {
		Vector2 touchPosition = OVRInput.Get (OVRInput.Axis2D.PrimaryTouchpad);
		float deadzone = 0.6f;
		//Up on dpad
		if (droneCamera.depth == 1) {
			if ((touchPosition.y > deadzone) && (Mathf.Abs (touchPosition.x) < 1 - deadzone)) {
				//move forward
				transform.position = transform.position + droneCamera.transform.forward * 5 * Time.deltaTime;
			} 
		}
	}
}
