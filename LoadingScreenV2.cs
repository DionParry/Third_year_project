﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScreenV2 : MonoBehaviour {
	
	public Slider slider;
	float progress;
	bool loaded = false;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () {
		StartCoroutine (Loading ());
	}
		
	/// <summary>
	/// Loading selected level
	/// Display progress as UI bar
	/// </summary>
	IEnumerator Loading() {
		while (loaded == false) {
			progress += Time.deltaTime / 2;
			slider.value = progress;
			if (progress >= 1.0f) {
				slider.value = 1.0f;
				loaded = true;
				SceneManager.LoadScene (LevelSelector.sceneNumber);
				//SceneManager.LoadScene (2);
			}
			yield return null;
		}
	}

}
