﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class JumpManager : MonoBehaviour
{

	//Enum to hold types of jump available
	public enum JumpMode
	{
		Basic,
		Charge,
		Thrust}

	;

	public JumpMode activeJump;
	private JumpMode current;
	public Text activeJumpText;
	public Image image;

	float progress;
	float timePressed = 0f;
	bool touching = false;
	Rigidbody rb;
	float thrust = 1f;
	int toggle = 0;
	bool isGrounded;
	bool flag = false;
	public bool abilityActive = false;
	bool refueling = false;

	// Use this for initialization
	void Start ()
	{
		activeJump = JumpMode.Basic;
		rb = GetComponent<Rigidbody> ();
		image.GetComponent<Image> ().fillAmount = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		OVRInput.Update ();
		TouchPad ();
		ToggleActiveJump ();
		Refuel ();
		activeJumpText.text = ("Active Jump: " + activeJump);

		//fill UI element based on jump parameters and type
		if (activeJump == JumpMode.Thrust) {
			image.GetComponent<Image> ().fillAmount = thrust;
		}
		if (activeJump == JumpMode.Basic) {
			image.GetComponent<Image> ().fillAmount = 0;
		}
	}

	void OnCollisionStay ()
	{
		isGrounded = true;
	}

	/*
	 * Touchpad setup 
	*/
	void ToggleActiveJump ()
	{
		if (OVRInput.GetActiveController () == OVRInput.Controller.LTrackedRemote ||
		    OVRInput.GetActiveController () == OVRInput.Controller.RTrackedRemote) {
			// yes, are they touching the touchpad?
			if (OVRInput.Get (OVRInput.Touch.PrimaryTouchpad)) {
				// yes, let's require an actual click rather than just a touch.
				if (OVRInput.Get (OVRInput.Button.PrimaryTouchpad)) {
					// button is depressed, handle the touch.
					Vector2 touchPosition = OVRInput.Get (OVRInput.Axis2D.PrimaryTouchpad);
					float deadzone = 0.6f;
					//Only allow active when ability pick up has been aquired.
					//if (pickUp == true) {
					if (OVRInput.Get (OVRInput.Button.PrimaryIndexTrigger)) {
						abilityActive = true;
						if ((touchPosition.x < -deadzone) && (Mathf.Abs (touchPosition.y) < 1 - deadzone)) {
							if (flag == false) {
								flag = true;
								ActiveJumpToggle ();
							}
						}
					} else { 
						abilityActive = false; //---
					}
				} else {
					flag = false;
				}
			}
		}
	}

	/*
	 * Each call
	 * toggle increments to cycles through jump mechanic
	 * activeJump is set as new jump type.
 	*/
	void ActiveJumpToggle ()
	{
		if (toggle > 2) { //reset position
			toggle = 0;
			//		flag = true;
		}
		if (toggle == 0) { //left position
			current = JumpMode.Basic;
			//		flag = true;
		}
		if (toggle == 1) { //right position
			//		flag = true;
			current = JumpMode.Charge;
		}
		if (toggle == 2) { //middle position
			current = JumpMode.Thrust;
			//image.GetComponent<Image> ().fillAmount = thrust;
		}
		activeJump = current;
		toggle++;
	}

	void TouchPad ()
	{
		if (OVRInput.GetActiveController () == OVRInput.Controller.LTrackedRemote ||
		    OVRInput.GetActiveController () == OVRInput.Controller.RTrackedRemote) {
			// yes, are they touching the touchpad?
			if (OVRInput.Get (OVRInput.Touch.PrimaryTouchpad)) {
				// yes, let's require an actual click rather than just a touch.
				if (OVRInput.Get (OVRInput.Button.PrimaryTouchpad)) {
					touching = true;
					// button is depressed, handle the touch.
					Vector2 touchPosition = OVRInput.Get (OVRInput.Axis2D.PrimaryTouchpad);
					float deadzone = 0.6f;
					//Only allow active when ability pick up has been aquired.
					//down on dpad
					if ((touchPosition.y < deadzone) && (Mathf.Abs (touchPosition.x) < 1 - deadzone)) {
						timePressed += Time.deltaTime;
						//Boost jump
						if (activeJump == JumpMode.Charge) {
							if (timePressed <= 5) {
								image.GetComponent<Image> ().fillAmount = timePressed;
							}
						} 

						//Jetpack
						if (activeJump == JumpMode.Thrust) {
							if (refueling == false) {
								if (thrust >= 0) {
									thrust -= Time.deltaTime / 2;
									rb.AddForce ((transform.up * 0.25f), ForceMode.Impulse);
								}
							}
							if (thrust <= 0) {
								StartCoroutine (fillTime ());
							}
						}
					}
				} else {
					//user has let go of touchpad, were they pressing down last frame? if so, process:
					if (timePressed > 0) {
						if (activeJump == JumpMode.Charge) {
							if (isGrounded == true) {
								image.GetComponent<Image> ().fillAmount = 0;
								rb.AddForce (Vector3.up * 10 * timePressed, ForceMode.Impulse);
								isGrounded = false;
							}
						}

						if (activeJump == JumpMode.Basic) {
							if (isGrounded == true) {
								//add jump force later..
								image.GetComponent<Image> ().fillAmount = 0;
								rb.AddForce (Vector3.up * 5, ForceMode.Impulse);
								isGrounded = false;
							}
						}

						//particles.Pause ();
						touching = false;
						timePressed = 0;
					}
				}
			}
		}
	}

	/*
	 * Reset fuel value if thurst is empty
 	*/
	void Refuel() {
		if (refueling == true && thrust <= 1) {
			thrust += Time.deltaTime;
			if (thrust >= 1) {
				refueling = false;
			}
		} 
	}

	/*
	 * Wait for 3 seconds before refueling
 	*/
	IEnumerator fillTime() {
		yield return new WaitForSeconds (3.0f);
		//thrust = 1f;
		refueling = true;
	}
}
