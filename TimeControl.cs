﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VR;

public class TimeControl : MonoBehaviour {

	public Image image;
	public Text timeControlAbilityText;
	private float countdown = 2;
	private float time;
	bool go = false;
	public bool TimeControlPickUp =  false;
	public RawImage TimeControlIcon;
	public RawImage TimeControlIconShaded;
	public float timeControlValue;


	// Use this for initialization
	void Start () {
		//image.enabled = false;
		go = false;
		image.GetComponent<Image> ().fillAmount = 0;
		TimeControlIcon.enabled = false;
	}

	void TouchPad ()
	{
		if (OVRInput.GetActiveController () == OVRInput.Controller.LTrackedRemote ||
			OVRInput.GetActiveController () == OVRInput.Controller.RTrackedRemote) {
			// yes, are they touching the touchpad?
			if (OVRInput.Get (OVRInput.Touch.PrimaryTouchpad)) {
				// yes, let's require an actual click rather than just a touch.
				if (OVRInput.Get (OVRInput.Button.PrimaryTouchpad)) {
					// button is depressed, handle the touch.
					TouchPadSetup ();
				}	
			}
		}
	}

	void TouchPadSetup() {
		Vector2 touchPosition = OVRInput.Get (OVRInput.Axis2D.PrimaryTouchpad);
		float deadzone = 0.6f;
		//Only allow active when ability pick up has been aquired.
		if (TimeControlPickUp == true) {
			timeControlAbilityText.text = ("TimeControl Ability: Active");
			if (OVRInput.Get (OVRInput.Button.PrimaryIndexTrigger)) {
				//down on dpad
				if ((touchPosition.y < deadzone) && (Mathf.Abs (touchPosition.x) < 1 - deadzone)) {
					go = true;
					time = countdown;
				}
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (go == true) {
			freeze ();
		}
		OVRInput.Update ();
		TouchPad ();

		keyController();
	}

	void keyController() {
		if (Input.GetKeyDown(KeyCode.L)) {
			go = true;
			time = countdown;
		}
	}

	//Pickup to activate the script
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag ("TimeSkip")) {
			other.gameObject.SetActive (false);
			TimeControlPickUp = true;
			TimeControlIconShaded.enabled = false;
			TimeControlIcon.enabled = true;
		}
	}

	public void freeze() {
		image.GetComponent<Image> ().fillAmount = 5;
		if (time > 0) {
			time -= Time.unscaledDeltaTime;
			image.GetComponent<Image> ().fillAmount = time / countdown;
			Time.timeScale = timeControlValue;

			//Time.timeScale = 0.0f;
		} else {
			go = false;
			Time.timeScale = 1.0f;
			Time.fixedDeltaTime = 0.02F * Time.timeScale;
			//new added code
			image.GetComponent<Image> ().fillAmount = 0;
		}
	}

}
