﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sidestep : MonoBehaviour
{

	public Transform cam;
	public Vector3 cameraRelativeRight;
	public Vector3 cameraRelativeLeft;

	// Use this for initialization
	void Start ()
	{
		cam = Camera.main.transform;
	}

	/// <summary>
	/// Update position of this instance.
	/// </summary>
	void Update ()
	{
		cameraRelativeLeft = cam.TransformDirection (Vector3.left);
		cameraRelativeRight = cam.TransformDirection (Vector3.right);
	}

	/// <summary>
	/// Moves the player left.
	/// </summary>
	public void MoveLeft() {
		transform.Translate(cameraRelativeLeft * 0.5f * Time.deltaTime);
	}

	/// <summary>
	/// Moves the player right.
	/// </summary>
	public void MoveRight() {
		transform.Translate(cameraRelativeRight * 0.5f * Time.deltaTime);
	}
}
