﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraSwitch : MonoBehaviour {

	public Camera mainCamera;
	public Camera droneCamera;
	public Camera uiCamera;
	public GameObject filter;
	bool DronePickUp = false;


	[Header("Drone ability Icons")]
	public RawImage DroneIcon;
	public RawImage DroneIconShaded;
	
	// Use this for initialization
	void Start () {
		DroneIcon.enabled = false;
		filter.SetActive(false);
		droneCamera.enabled = false;
	}

	//Pickup to activate the script
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag ("DronePickUp")) {
			other.gameObject.SetActive (false);
			DronePickUp = true;
			DroneIconShaded.enabled = false;
			DroneIcon.enabled = true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		swap ();
	}
	/*
	 * Game uses the camera with highest depth
	 * when using keyboard.
	*/
	void swap ()
	{
		if (Input.GetKeyDown (KeyCode.P)) {		 
			//drone Camera
			if (mainCamera.depth == 1 && uiCamera.depth == 1) {
				mainCamera.depth = 0;
				uiCamera.depth = 0;
				droneCamera.depth = 1;
				droneCamera.enabled = true;
				uiCamera.enabled = false;
				mainCamera.enabled = false;
				filter.SetActive(true);
			} 
			//Main Camera
			else { 
				filter.SetActive(false);
				mainCamera.depth = 1;
				uiCamera.depth = 1;
				droneCamera.depth = 0;
				droneCamera.enabled = false;
				mainCamera.enabled = true;
				uiCamera.enabled = true;
			}
		}
	}

	/*
	 * Game uses the camera with highest depth
	*/
	public void Switch() {
		//drone Camera
		if (DronePickUp == true) {
			if (mainCamera.depth == 1 && uiCamera.depth == 1) {
				mainCamera.depth = 0;
				uiCamera.depth = 0;
				droneCamera.depth = 1;

				droneCamera.enabled = true;
				uiCamera.enabled = false;
				mainCamera.enabled = false;
				filter.SetActive (true);
			}
		//Main Camera
		else {
				filter.SetActive (false);
				mainCamera.depth = 1;
				uiCamera.depth = 1;
				droneCamera.depth = 0;

				droneCamera.enabled = false;
				mainCamera.enabled = true;
				uiCamera.enabled = true;
			}
		}
	}
}