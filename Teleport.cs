﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Teleport : MonoBehaviour
{
	public Camera playerCamera;
	private RaycastHit hit;
	float defaultFov;
	float time = 0;
	public bool startTeleport = false;
	public RawImage teleportIcon;
	public RawImage teleportIconShaded;
	public bool teleportPickUp = false;

	void Start ()
	{
		defaultFov = playerCamera.fieldOfView;
		teleportIcon.enabled = false;
	}

	void Update ()
	{
		TestKeyBoard ();
		//if (startTeleport == true) {
		//	TeleportationEffect ();
		//}
	}

	/// <summary>
	/// Raises the trigger enter event.
	/// Checks for collision to activate ability.
	/// </summary>
	/// <param name="other">Other.</param>
	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.CompareTag ("Teleport")) {
			other.gameObject.SetActive (false);
			teleportPickUp = true;
			teleportIconShaded.enabled = false;
			teleportIcon.enabled = true;
		}
	}

	/// <summary>
	/// Teleportation on specified range.
	/// Exectues teleport ability based on boolean
	/// </summary>
	/// <param name="range">Range.</param>
	public void Teleportation (float range)
	{
		if (teleportPickUp == true) {
			Ray ray = Camera.main.ViewportPointToRay (new Vector3 (0.5f, 0.5f, 0));
			if (Physics.Raycast (ray, out hit, range)) {
				transform.position = hit.point + hit.normal;
				AudioSource audio = GetComponent<AudioSource> ();
				audio.Play ();
				playerCamera.fieldOfView = defaultFov;
			}
		}
	}

	/// <summary>
	/// Teleportations effect.
	/// Create the warping effect for the teleport
	/// </summary>
	public void TeleportationEffect ()
	{
		if (time <= 2) {
			time += Time.deltaTime;
			playerCamera.fieldOfView -= Time.deltaTime * 10;
			if (time >= 2) {
				Teleportation (50);
			}
		} else {
			startTeleport = false;
			time = 0;
		}
	}

	/// <summary>
	/// Tests the key board.
	/// Starts the effect by setting as true
	/// </summary>
	void TestKeyBoard ()
	{
		if (Input.GetKeyDown (KeyCode.T)) {
			//startTeleport = true;
			teleportPickUp = true;
			Teleportation (50);
		}
	}
}