﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallRun : MonoBehaviour
{

	private bool wallR = false;
	private bool wallL = false;
	RaycastHit hitR;
	RaycastHit hitL;
	public ParticleSystem wallRunParticles;

	Rigidbody rb;
	bool isGrounded = false;

	/// <summary>
	/// Raises the collision stay event.
	/// </summary>
	void OnCollisionStay ()
	{
		isGrounded = true;
	}

	// Use this for initialization
	void Start ()
	{
		wallRunParticles.Pause ();
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		WallRunLeft ();
		WallRunRight ();
	}

	/// <summary>
	/// Checks contacts with wall on the left.
	/// </summary>
	void WallRunLeft() {

		if (Physics.Raycast (transform.position, -transform.right, out hitL, 1)) {
			if (hitL.transform.tag == "Wall") {
				wallR = false;
				wallL = true;
				rb.useGravity = false;
				wallRunParticles.Play ();
				rb.constraints = RigidbodyConstraints.FreezePositionY;
				rb.constraints = RigidbodyConstraints.FreezeRotation;	
				StartCoroutine (afterRun ());
			}
		} else {
			rb.useGravity = true;
		}
	}

	/// <summary>
	/// Checks contacts with wall on the right.
	/// </summary>
	void WallRunRight() {
		if (Physics.Raycast (transform.position, transform.right, out hitR, 1)) {
			if (hitR.transform.tag == "Wall") {
				wallR = true;
				wallL = false;
				rb.useGravity = false;
				wallRunParticles.Play ();
				rb.constraints = RigidbodyConstraints.FreezePositionY;
				rb.constraints = RigidbodyConstraints.FreezeRotation;	
				StartCoroutine (afterRun ());
			}
		} else {
			rb.useGravity = true;
		}
	}

	/// <summary>
	/// After 3.5 seconds, reset gravity
	/// </summary>
	/// <returns>The run.</returns>
	IEnumerator afterRun ()
	{
		yield return new WaitForSeconds (3.5f);
		wallL = false;
		wallR = false;
		rb.useGravity = true;
	}
}
