﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelector : MonoBehaviour
{
	public bool lockLV2;

	private RaycastHit hit;
	public LineRenderer lr;
	public Camera cam;
	float timer = 0;
	float time = 0;
	float range = 25f;
	public GameObject panel1;
	public GameObject panel2Shaded;
	public GameObject panel2Text;
	public Image loadingCircle;
	public Text debugText;
	public static int sceneNumber;

	// Use this for initialization
	void Start ()
	{
		loadingCircle.fillAmount = 0;
		//LookForPanel ();
		//lr = GetComponent<LineRenderer> ();
		//cam = GetComponent<Camera>();
		//lv1Controller = GameObject.FindObjectOfType<LevelController> ();
	}

	// Update is called once per frame
	void Update ()
	{
		//PanelSelect ();
		//buttonTester ();
		LookForPanel ();
		if (lockLV2 != true) {
			if (LevelController.level1Completed == true) {
				panel2Shaded.SetActive (false);
				panel2Text.SetActive (false);
				LookForPanel2 ();
			}
		}
	}


	/// <summary>
	/// Looks for panel.
	/// If found, scene number = 2.
	/// </summary>
	public void LookForPanel ()
	{
		//create ray and look for collider with tag..
		Ray ray = Camera.main.ViewportPointToRay (new Vector3 (0.5F, 0.5F, 0));
		if (Physics.Raycast (ray, out hit, range)) {
			if (hit.collider.tag == "Level1") {
				//debugText.text = "Looking at panel 1";
				//start loading bar, once done load scene
				time += Time.unscaledDeltaTime;
				loadingCircle.fillAmount = time / 2;
				if (loadingCircle.fillAmount >= 1) {
					//pass scene to load to the loading screen
					sceneNumber = 2;
					//Testing scene 2;
					//sceneNumber = 3;
					LoadByIndex (1);

				}
			}
		} else {
			loadingCircle.fillAmount = 0;
			time = 0;
			//debugText.text = "";

		}
	}

	/// <summary>
	/// Looks for panel.
	/// If found, scene number = 3.
	/// </summary>
	public void LookForPanel2 ()
	{
		//create ray and look for collider with tag..
		Ray ray = Camera.main.ViewportPointToRay (new Vector3 (0.5F, 0.5F, 0));
		if (Physics.Raycast (ray, out hit, range)) {
			if (hit.collider.tag == "Level2") {
				//debugText.text = "Looking at panel 2";
				time += Time.unscaledDeltaTime;
				loadingCircle.fillAmount = time / 2;
				if (loadingCircle.fillAmount >= 1) {
					sceneNumber = 3;
					LoadByIndex (1);
				}
			} 
		} else {
			//debugText.text = "";
			loadingCircle.fillAmount = 0;
			time = 0;
		}
	}
		
	/// <summary>
	/// Loads the index of the level.
	/// </summary>
	/// <param name="sceneIndex">Scene index.</param>
	public void LoadByIndex (int sceneIndex)
	{
		SceneManager.LoadScene (sceneIndex);
	}

	/// <summary>
	/// Testing PanelEnlarge().
	/// </summary>
	void buttonTester ()
	{
		if (Input.GetKey (KeyCode.L)) {
			PanelEnlarge ();
		}
	}

	//When panel is looked at
	//appear bigger
	/// <summary>
	/// Change size of the Panels.
	/// </summary>
	void PanelEnlarge ()
	{
		//panel1.transform.localScale = new Vector3 (2.5f, 0, 2.5f);
		//panel1.transform.localScale = new Vector3 (panel1.transform.localScale.x + 2, 0, panel1.transform.localScale.z + 2 );
		panel1.transform.localPosition = new Vector3 (0.5f, 0, 1);
	}
}

