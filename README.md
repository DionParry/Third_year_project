Bangor Uni final year project.

For my final year project, I decided to choose VR as my specialist topic.
With guidance from an experienced VR developer LLyr Ap Cenydd and whilst adhearing
to VR Best Practices, ProjectEMM was created to test movement mechanics within 
VR and see how far they can be explored.
_________________
Goals

-Follow VR best practices and other resources to create a user friendly
experience throughout.
-Test various forms of movement mechanics 
-Developing an interactive start menu
-Simple but effective level design which rewards the user by using the correct
movement mechanic available to complete the task.

____________________
Result

By the end of the project, a fully working .apk was developed for android for 
users to test.
Testing was done using the SUS scale to provide feedback on the system.
Once deployed to android further testing was carried out in a local school, 
these students provided the data for the SUS scale mentioned previously.