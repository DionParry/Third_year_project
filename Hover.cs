﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour {

	public Camera droneCamera;
	public GameObject DronePosition1;
	Transform pos1;
	Transform player;

	float timer = 3;
	Vector3 point;
	int move = 0;
	public float rotationSpeed;
	public float moveSpeed;


	// Use this for initialization
	void Start () {
		//find the player its tag
		player = GameObject.FindGameObjectWithTag ("Player").transform; 
		//target destination area
		pos1 = DronePosition1.transform;
	}
	
	// Update is called once per frame
	void Update () {
		//Makes a timer to change position after reaching newPosition time
		timer += Time.deltaTime;

		//Controls when method can be activated
		if (droneCamera.depth == 0) {
			moveToTarget ();
			/*
			if (timer >= 5) {
				newPosition ();
				timer = 0; //reset timer
			}
			*/
		}
	}

	/*
	 * Moves the object towards the player
 	*/
	void moveToTarget() {
		/* Look at Player */
		transform.rotation = Quaternion.Slerp (transform.rotation , Quaternion.LookRotation (pos1.position - transform.position) , rotationSpeed * Time.deltaTime);
		// move towards player
		transform.position += transform.forward * moveSpeed * Time.deltaTime;
	}


	/*
	 * Each time newPosition() is called
	 * Destination cycles through to next location
	 * Vector 3 point, is set as new destination
 	*/
	void newPosition() {
		if (move > 2) { //reset position
			move = 0;
		} if (move == 0) { //left position
			point = new Vector3 (player.position.x + 0.5f, player.position.y + 1f, player.position.z - 0.5f);
		} if (move == 1) { //right position
			point = new Vector3 (player.position.x + 1.5f, player.position.y + 1f, player.position.z - 0.5f);
		} if (move == 2) { //middle position
			point = new Vector3 (player.position.x + 1f, player.position.y + 1f, player.position.z + 0.5f);
		}
		transform.position = point;
	
		//addd rigidbody remove gravity off
		//thrust in direction of position
		move++;
		//Debug.Log (move);
	}
}