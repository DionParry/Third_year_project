﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class LevelController : MonoBehaviour
{
	//-2 right, -14 left
	Teleport t;
	public GameObject BlockAProgressWall;
	public GameObject BlockAText;
	public GameObject BlockAExitText;
	public GameObject BlockBProgressWall;
	public GameObject BlockBText;
	public GameObject BlockCProgressWall;
	public GameObject BlockCText;
	public GameObject BlockDProgressWall;
	public GameObject BlockDText;
	public GameObject BlockEProgressWall;
	public GameObject BlockEText;
	public GameObject BlockFText;

	[Header("Moving Pad settings")]
	public GameObject movingPad;
	public Vector3 pos1 = new Vector3();
	public Vector3 pos2 = new Vector3();
	public float moveSpeed;

	[Header("Exit level")]
	public GameObject ExitPanel;
	public static bool level1Completed;
	//Vector3 pos1 = new Vector3(1f,7f,-1f);
	//Vector3 pos2 = new Vector3(14f,7f,-1f);

	// Use this for initialization
	/// <summary>
	/// Turn all other text other than current room off
	/// </summary>
	void Start ()
	{
		t = GameObject.FindObjectOfType<Teleport> ();
		BlockBText.SetActive (false);
		BlockCText.SetActive (false);
		BlockDText.SetActive (false);
		BlockEText.SetActive (false);
		BlockFText.SetActive (false);
		level1Completed = false;
	}

	// Update is called once per frame
	/// <summary>
	/// Update position of this instance.
	/// </summary>
	void Update ()
	{
		movingPad.transform.position = Vector3.Lerp (pos1, pos2, Mathf.PingPong(Time.time*moveSpeed, 1.0f));
	}


	/// <summary>
	/// Pickup to activate the script
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="other">Other.</param>
	void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag ("BlockAProgressCube")) {
			other.gameObject.SetActive (false);
			BlockAProgression ();
		}
		if (other.gameObject.CompareTag ("BlockBProgressCube")) {
			other.gameObject.SetActive (false);
			BlockBProgression ();
		}
		if (other.gameObject.CompareTag ("BlockCProgressCube")) {
			other.gameObject.SetActive (false);
			BlockCProgression ();
		}
		if (other.gameObject.CompareTag ("BlockDProgressCube")) {
			other.gameObject.SetActive (false);
			BlockDProgression ();
		}
		if (other.gameObject.CompareTag ("BlockEProgressCube")) {
			other.gameObject.SetActive (false);
			BlockEProgression ();
		}
		if (other.gameObject.CompareTag ("Exit Plane")) {
			SceneManager.LoadScene (0);
			level1Completed = true;
		}

	}

	/// <summary>
	/// Blocks A progression.
	/// </summary>
	void BlockAProgression() {
		BlockAProgressWall.SetActive (false);
		BlockBText.SetActive (true);
		BlockAText.SetActive (false);
		BlockAExitText.SetActive (false);
	}

	/// <summary>
	/// Blocks the B progression.
	/// </summary>
	void BlockBProgression() {
		BlockBProgressWall.SetActive (false);
		BlockCText.SetActive (true);
		BlockBText.SetActive (false);
	}

	/// <summary>
	/// Blocks the C progression.
	/// </summary>
	void BlockCProgression() {
		BlockCProgressWall.SetActive (false);
		BlockDText.SetActive (true);
		BlockCText.SetActive (false);
	}

	/// <summary>
	/// Blocks the D progression.
	/// </summary>
	void BlockDProgression() {
		BlockDProgressWall.SetActive (false);
		BlockEText.SetActive (true);
		BlockDText.SetActive (false);
	}
		
	/// <summary>
	/// Blocks the E progression.
	/// </summary>
	void BlockEProgression() {
		BlockEProgressWall.SetActive (false);
		BlockFText.SetActive (true);
		BlockEText.SetActive (false);
	}

}