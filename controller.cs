﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class controller : MonoBehaviour
{
	public Camera playerCamera;
	private Vector3 spawnPoint;
	public float speed;
	Jump j;
	Teleport t;
	CameraSwitch cs;
	Sidestep side;
	JumpManager jm;
	bool active = false;
	public Text teleportAbilityText;
	public Text debugText;
	bool flag = false;
	Rigidbody rb;

	// Use this for initialization
	void Start ()
	{
		spawnPoint = transform.position; //sets the spawn point to the current starting position
		//calls scripts
		t = GameObject.FindObjectOfType<Teleport> ();
		cs = GameObject.FindObjectOfType<CameraSwitch> ();
		side = GameObject.FindObjectOfType<Sidestep> ();
		jm = GameObject.FindObjectOfType<JumpManager> ();
		rb = GetComponent<Rigidbody> ();
	}

	
	// Update is called once per frame
	void Update ()
	{
		OVRInput.Update ();
		TouchPad ();
		ResetSpawn ();
		TestKeyBoard ();
	}

	void ResetSpawn ()
	{
		if (transform.position.y < -10f) {
			transform.position = spawnPoint; //once fallen below -10 reset spawn
		}
	}

	/// <summary>
	/// Tests the key board and following:
	/// Forward movement
	/// Sidestep
	/// Teleport
	/// </summary>
	void TestKeyBoard ()
	{
		if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
			if (playerCamera.depth == 1) {
				transform.Translate (side.cameraRelativeLeft * 5 * Time.deltaTime);
			}
		}
		if (Input.GetKeyDown (KeyCode.T)) {
			t.startTeleport = true;
		}
		if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
			if (playerCamera.depth == 1) {
				transform.Translate (side.cameraRelativeRight * 5 * Time.deltaTime);
			}
		}
		if (Input.GetKey (KeyCode.UpArrow) || Input.GetKey (KeyCode.W)) {
			if (playerCamera.depth == 1) {
				transform.position = transform.position + Camera.main.transform.forward * speed * Time.deltaTime;
			}
		}
		if (Input.GetKey (KeyCode.DownArrow) || Input.GetKey (KeyCode.S)) {
			if (playerCamera.depth == 1) {
				transform.position = transform.position + -Camera.main.transform.forward * speed * Time.deltaTime;
			}
		}
	}
		
	/// <summary>
	/// Touchpad setup 
	/// </summary>
	void TouchPad ()
	{
		if (OVRInput.GetActiveController () == OVRInput.Controller.LTrackedRemote ||
		    OVRInput.GetActiveController () == OVRInput.Controller.RTrackedRemote) {
			// yes, are they touching the touchpad?
			if (OVRInput.Get (OVRInput.Touch.PrimaryTouchpad)) {
				// yes, let's require an actual click rather than just a touch.
				if (OVRInput.Get (OVRInput.Button.PrimaryTouchpad)) {
					// button is depressed, handle the touch.
					TouchPadSetup ();
				} else {
					//used to toggle as frame
					flag = false;
				}
			}
		}
	}

	/// <summary>
	/// Setup is defined
	/// Movement mechanics are set to position on controller
	/// </summary>
	void TouchPadSetup ()
	{
		Vector2 touchPosition = OVRInput.Get (OVRInput.Axis2D.PrimaryTouchpad);
		float deadzone = 0.6f;

		//Up on dpad = move forward
		if ((touchPosition.y > deadzone) && (Mathf.Abs (touchPosition.x) < 1 - deadzone)) {
			active = true;
			if (playerCamera.depth == 1) {
				transform.position = transform.position + Camera.main.transform.forward * speed * Time.deltaTime;
			}
		} else {
			active = false;
		}

		//left on dpad = strafe left
		if ((touchPosition.x < -deadzone) && (Mathf.Abs (touchPosition.y) < 1 - deadzone)) { //changed < 1-
			active = true;
			if (playerCamera.depth == 1) {
				if (jm.abilityActive != true) {
					side.MoveLeft ();
				}
			} else {
				active = false;
			}
		}

		//Right on dpad = strafe right
		if ((touchPosition.x > deadzone) && (Mathf.Abs (touchPosition.y) < 1 - deadzone)) {
			active = true;
			if (playerCamera.depth == 1) {
				if (jm.abilityActive != true) {
					side.MoveRight ();
				}
			} else {
				active = false;
			}
		}

		//Right on dpad + trigger = switch camera
		if (OVRInput.Get (OVRInput.Button.PrimaryIndexTrigger)) {
			if ((touchPosition.x > deadzone) && (Mathf.Abs (touchPosition.y) < 1 - deadzone)) {
				active = true;
				if (flag == false) {
					//button can only be pressed once per call
					flag = true;
					cs.Switch ();
				}
			} else {
				active = false;
			}
		}

		//Front Trigger + upDpad  = teleport
		if (OVRInput.Get (OVRInput.Button.PrimaryIndexTrigger)) {
			if ((touchPosition.y > deadzone) && (Mathf.Abs (touchPosition.x) < 1 - deadzone)) {
				active = true;
				t.Teleportation (50);
			} else {
				active = false;
			}
		}

		//reset button for level 1
		if (OVRInput.Get (OVRInput.Button.Back)) {
			//transform.position = spawnPoint;
			SceneManager.LoadScene (2);
		}
	}

}
